FROM python:3.7-slim-buster

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN pip3 install --no-cache-dir -r requirements.txt

CMD [ "python", "./prediction.py" ]
