# Image Recognition

## Prerequisites

 * python > 3.6 
 * put a file "sample.jpg" 

## Install

Install all dependencies and run predicition.py file
```sh
$ pip install -r requirements.txt
$ pip3 install https://github.com/OlafenwaMoses/ImageAI/releases/download/2.0.2/imageai-2.0.2-py3-none-any.whl
$ python prediction.py
```